/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.sample.resourceenum;

import java.util.Locale;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Test;
import org.vermeerlab.sample.resourceenum.config.Utf8;

/**
 *
 * @author Yamashita,Takahiro
 */
public class Utf8Test {

    @Test
    public void デフォルトロケールを参照() {
        Assert.assertThat(Utf8.UFT8_001.toString(), is("UTF8コードで作成したファイル"));
    }

    @Test
    public void 存在しないロケールを指定した場合はルートロケール() {
        Assert.assertThat(Utf8.UFT8_001.format(Locale.ITALIAN), is("UTF8コードで作成したファイル"));
    }

}
